import apple from "./download.jpg";
function addImage() {
  const img = document.createElement("img");
  img.alt = "apple";
  img.width = 300;
  img.height = 250;
  img.src = apple;

  const body = document.querySelector("body");
  body.appendChild(img);
}

export default addImage;
