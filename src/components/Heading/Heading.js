//class represent button component !!

import "./heading.scss";

class Heading {
  //create render function which be reponsibel to create button
  //and append it to body element
  render() {
    const h1 = document.createElement("h1");
    const body = document.querySelector("body");

    h1.innerHTML = "Keep Progress , Hungry For Learing ";

    body.appendChild(h1);
  }
}

export default Heading;
